import Link from 'next/link'

const linkStyle = {
  marginRight: 15,
  backgroundColor: '#ddd',
  padding: '5px'
}

const Header = () => (
  <div>
    <Link href="/" title="Index Page">
      <a style={linkStyle}>Home</a>
    </Link>
    <Link href="/about" title="About Page">
      <a style={linkStyle}>About Page</a>
    </Link>
  </div>
)

export default Header