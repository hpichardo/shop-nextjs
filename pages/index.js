import { useEffect } from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styled from 'styled-components';
import Layout from '../components/Layout'
import { fetchProducts } from '../actions'

const Title = styled.h1`
  color: red;
`;

const Index = ({ products, getProducts }) => {

  useEffect(() => {
    document.title = "hello"
  }, []);

  return (
    <Layout>
      <Title>Shop Next.js Page</Title>
      <ul>
        {products && products.map(product => (
          <li key={product.id}>
            <Link as={`/products/${product.id}`} href={`/product?id=${product.id}`}>
              <a>{product.name}</a>
            </Link>
          </li>
        ))}
      </ul>
    </Layout>
  )
}




Index.getInitialProps = async function() {
  const products = await fetchProducts()

  console.log(`Show data fetched. Count: ${products}`)

  return {
    products
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    getProducts: bindActionCreators(fetchProducts, dispatch)
  }
};


export default connect(state => state, mapDispatchToProps)(Index)