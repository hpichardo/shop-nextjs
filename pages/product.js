import { withRouter } from 'next/router'
import styled from 'styled-components';
import Layout from '../components/Layout'
import fetch from 'isomorphic-unfetch'

const Product = withRouter(({ show }) => (
  <Layout>
    <Title>{show.name}</Title>
    <p>{show.summary.replace(/<[/]?p>/g, '')}</p>
    <img src={show.image.medium} />
  </Layout>
))


Product.getInitialProps = async function (context) {
  const { id } = context.query
  const res = await fetch(`https://api.tvmaze.com/shows/${id}`)
  const show = await res.json()

  return { show }
}

const Title = styled.h1`
  color: red;
`;


export default Product