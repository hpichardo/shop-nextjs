import { loadDB } from '../lib/db'

export const actionTypes = {
  SET_PRODUCTS: 'SET_PRODUCTS',
}


export const fetchProducts = () => async dispatch => {
  const db = await loadDB();
  console.log("fetch")

  let newState = {
    products: []
  };

  db.firestore().collection('products')
      .onSnapshot(snapshot => {

        console.log({snapshot})
        snapshot.forEach(function(product) {
          newState.products.push({
            id: product.id,
            product: product.data()
          });
        });
      });

      dispatch({
        type: actionTypes.SET_PRODUCTS,
        payload: newState
      })
};