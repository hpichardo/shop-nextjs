import firebase from '@firebase/app';
import '@firebase/firestore'

export function loadDB() {
  try {
    var config = {
      apiKey: "AIzaSyCs4kYzRZfsNbzhMx2FQ_l3cU4EGf6F6ko",
      authDomain: "store-98d19.firebaseapp.com",
      databaseURL: "https://store-98d19.firebaseio.com",
      projectId: "store-98d19",
      storageBucket: "store-98d19.appspot.com",
      messagingSenderId: "3579038431"
    };
    firebase.initializeApp(config);
  } catch (err) {
    // we skip the "already exists" message which is
    // not an actual error when we're hot-reloading
    if (!/already exists/.test(err.message)) {
      console.error('Firebase initialization error', err.stack);
    }
  }

  return firebase;
}