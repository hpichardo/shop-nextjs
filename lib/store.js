import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { actionTypes } from '../actions'


const InitialState = {
  products: [],
  light: false,
  count: 0
}

// REDUCERS
export const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case actionTypes.SET_PRODUCTS:
      console.log("action: ", action.payload)
      return Object.assign({}, state, {
        products: action.payload,
      })
    default:
      return state
  }
}


export function initializeStore (initialState = InitialState) {
  return createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware())
  )
}